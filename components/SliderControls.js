import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  Image,
  VrButton
} from 'react-vr';
import {
  ARROW_PREV_IMG_SRC,
  ARROW_NEXT_IMG_SRC
} from '../helpers/_constants';

export default class SliderControls extends Component {
  static propTypes = {
    onNextClick: PropTypes.func.isRequired,
    onPrevClick: PropTypes.func.isRequired,
    timeout: PropTypes.number
  };

  static defaultProps = {
    timeout: 0
  };

  state = {
    opacity: new Animated.Value(0)
  };

  _style = {
    controlsWrapper: {
      position: 'absolute',
      width: 0.26,
      left: 23.535,
      top: 0.05,
      opacity: this.state.opacity,
      transform: [{ translateZ: -1 }]
    },
    buttonPrev: {
      width: 0.1255,
      height: 0.0105
    },
    buttonNext: {
      width: 0.1255,
      height: 0.0105,
      marginLeft: 0.25,
      marginTop: -0.0105
    }
  };

  componentDidMount() {
    Animated.timing(this.state.opacity, {
      toValue: 1,
      duration: 1000,
      delay: this.props.timeout
    }).start();
  }

  render() {
    const { _style, props } = this;

    return (
      <Animated.View style={_style.controlsWrapper}>
        <VrButton onClick={props.onPrevClick}>
          <Image
            style={_style.buttonPrev}
            source={{
              uri: ARROW_PREV_IMG_SRC
            }}
          />
        </VrButton>
        <VrButton onClick={props.onNextClick}>
          <Image
            style={_style.buttonNext}
            source={{
              uri: ARROW_NEXT_IMG_SRC
            }}
          />
        </VrButton>
      </Animated.View>
    );
  }
}
