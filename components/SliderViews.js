import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated, NativeModules } from 'react-vr';
import SliderSlide from './SliderSlide';

const { PreloaderModule } = NativeModules;

export default class SliderViews extends Component {
  static propTypes = {
    slides: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      url: PropTypes.string.isRequired,
      text: PropTypes.string,
      type: PropTypes.oneOf(['image', 'video']),
      poster: PropTypes.string,
      audio: PropTypes.string,
      onLoad: PropTypes.func
    })).isRequired,
  };

  _slideInitialPosition = 22.91;

  _slideWidth = 1.9;

  _slidesArray = [];

  state = {
    translateX: new Animated.Value(this._slideInitialPosition)
  };

  _style = {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    transform: [
      { translateX: this.state.translateX },
      { translateZ: -1.6 }
    ]
  };

  /**
   * Get slide instance with specified id
   *
   * @public
   * @param {Number} slideId
   * @return {Object}
   */
  getSlide(slideId) {
    return this._slidesArray[slideId];
  }

  /**
   * Generate slides array
   *
   * @private
   * @get
   * @return {Array<XML>}
   */
  get _views() {
    const {
      _slidesArray,
      _slideWidth,
      _slideInitialPosition,
      props: { slides }
    } = this;

    return slides.map(({ id: key, url: src, text, type, audio, poster }, i) => {
      const slideSettings = {
        key,
        src,
        text,
        type,
        audio,
        poster,
        ref: slide => _slidesArray.push(slide),
        position: _slideInitialPosition - (i * _slideWidth)
      };
      const isLast = (i + 1) === slides.length;

      if (isLast) {
        slideSettings.onLoad = () => PreloaderModule.finishPreloaderAndRunInstruction();
      }

      return <SliderSlide {...slideSettings} />;
    });
  }

  render() {
    const { _views, _style } = this;

    return (
      <Animated.View style={_style}>
        {_views}
      </Animated.View>
    );
  }
}
