import React, { Component } from 'react';
import { View } from 'react-vr';
import Background from './Background';
import Slider from './Slider';

export default class App extends Component {
  _style = {
    appWrapper: {
      layoutOrigin: [0.5, 0.5]
    }
  };

  _sliderSettings = {
    slides: require('../json/slides.json'),
    initialSlide: 10
  };

  render() {
    const { _style, _sliderSettings } = this;

    return (
      <View style={_style.appWrapper}>
        <Background />
        <Slider {..._sliderSettings} />
      </View>
    );
  }
};
