import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Easing } from 'react-native';
import {
  View,
  Animated,
  VrHeadModel,
  NativeModules
} from 'react-vr';
import SliderViews from './SliderViews';
import SliderControls from './SliderControls';

const { ScreenModule } = NativeModules;

export default class Slider extends Component {
  static propTypes = {
    slides: PropTypes.arrayOf(PropTypes.object).isRequired,
    initialSlide: PropTypes.number,
    changeSlideOnRotate: PropTypes.bool,
    changeSlideOnRotateInterval: PropTypes.number,
    sliderSpeed: PropTypes.number
  };

  static defaultProps = {
    initialSlide: 1,
    changeSlideOnRotate: ScreenModule.isMobile,
    changeSlideOnRotateInterval: 25,
    sliderSpeed: 1300
  };

  _currentSlide = 1;

  _centered = true;

  _changeSlideOnRotateListener = null;

  _changeSlideOnRotatePause = 1500;

  componentDidMount() {
    if (this.props.changeSlideOnRotate) this._changeSlideOnRotate('run');

    this.goToSlide(this.props.initialSlide);
  }

  /**
   * Navigate to specified slide
   *
   * @public
   * @this Slider
   * @param {String|Number} goToSlideId - 'next', 'prev' or slide id
   */
  goToSlide = (goToSlideId) => {
    const { _currentSlide, props: { slides } } = this;
    const slidesCount = slides.length - 1;
    const isNext = goToSlideId === 'next';
    const isPrev = goToSlideId === 'prev';
    const slideId =
          isNext ? _currentSlide + 1
        : isPrev ? _currentSlide - 1
        : goToSlideId;

    // trying to activate already active slide
    if (goToSlideId === _currentSlide) return;

    // it was the last slide, go to the first one now
    if (isNext && _currentSlide === slidesCount) {
      return this.goToSlide(0);
    }

    // it was the first slide, go to the last one now
    if (isPrev && _currentSlide === 0) {
      return this.goToSlide(slidesCount);
    }

    // change slides
    this._changeSlide(slideId);
  };

  /**
   * Change active slide (run slider and slides animation)
   *
   * @private
   * @param {Number} slideId
   */
  _changeSlide(slideId) {
    const { _SliderViews, props } = this;
    const slidePosition = _SliderViews.getSlide(slideId).props.position;

    // animate slider to slide's position
    Animated.timing(_SliderViews.state.translateX, {
      toValue: slidePosition,
      duration: props.sliderSpeed,
      easing: Easing.bezier(0.175, 0.885, 0.32, 1.275)
    }).start();

    // show active slide's text and play video while pausing previous
    this._showActiveSlideLabelAndPlayVideo(slideId, this._currentSlide);

    // pause
    if (props.changeSlideOnRotate) this._changeSlideOnRotate('pauseAndReverse');

    // update active slide ID
    this._currentSlide = slideId;
  }

  /**
   * Animate active label opacity and play video (if possible)
   * Hide previous slide label and pause video
   *
   * @private
   * @param {Number} activeSlideId
   * @param {Number} prevSlideId
   */
  _showActiveSlideLabelAndPlayVideo(activeSlideId, prevSlideId) {
    const nextSlide = this._SliderViews.getSlide(activeSlideId);
    const prevSlide = this._SliderViews.getSlide(prevSlideId);

    // label
    Animated.parallel([
      Animated.spring(nextSlide.state.textOpacity, { toValue: 1, duration: 450 }),
      Animated.spring(prevSlide.state.textOpacity, { toValue: 0, duration: 350 })
    ]).start();

    // video
    nextSlide.state.playerState && nextSlide.state.playerState.play();
    prevSlide.state.playerState && prevSlide.state.playerState.pause();
  }

  /**
   * Check head rotation degree every N ms and change active slide
   * Possible actions: 'run', 'stop', 'pauseAndReverse'
   *
   * @private
   * @param {String} action
   */
  _changeSlideOnRotate(action) {
    if (!this.props.changeSlideOnRotate) return;

    switch (action) {
      case 'run': {
        this._changeSlideOnRotateListener = setInterval(
          this._checkHeadPositionAndChangeSlide,
          this.props.changeSlideOnRotateInterval
        );
        break;
      }
      case 'stop': {
        clearInterval(this._changeSlideOnRotateListener);
        break;
      }
      case 'pauseAndReverse': {
        this._changeSlideOnRotate('stop');
        this._changeSlideOnRotateListener = setTimeout(
          this._changeSlideOnRotate.bind(this, 'run'),
          this._changeSlideOnRotatePause
        );
        break;
      }
    }
  }

  /**
   * Check current HeadModel rotation and change slide if needed
   *
   * @private
   */
  _checkHeadPositionAndChangeSlide = () => {
    const [,XRotation] = VrHeadModel.rotationInRadians();

    // watching at the center slide, do nothing
    if (XRotation > -0.35 && XRotation < 0.35) {
      return this._centered = true;
    }

    // watching somewhere else, change slide
    if (this._centered) {
      this.goToSlide(XRotation > 0 ? 'prev' : 'next');
      this._centered = false;
    }

    // pause
    if (this.props.changeSlideOnRotate) this._changeSlideOnRotate('pauseAndReverse');
  };

  render() {
    const { props, goToSlide } = this;

    return (
      <View>
        {ScreenModule.isDesktop &&
        <SliderControls
          onPrevClick={() => goToSlide('prev')}
          onNextClick={() => goToSlide('next')}
        />}
        <SliderViews
          slides={props.slides}
          ref={(SliderViews) => this._SliderViews = SliderViews}
        />
      </View>
    );
  }
}
