import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  Animated,
  Video,
  Sound,
  MediaPlayerState,
  NativeModules
} from 'react-vr';

const { ScreenModule } = NativeModules;

export default class SliderSlide extends Component {
  static propTypes = {
    src: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['image', 'video']),
    text: PropTypes.string,
    poster: PropTypes.string,
    audio: PropTypes.string,
    onLoad: PropTypes.func
  };

  static defaultProps = {
    type: 'image',
    text: null,
    poster: null,
    audio: null,
    onLoad: null
  };

  state = {
    textOpacity: new Animated.Value(0),
    playerState: null
  };

  _style = {
    slideWrapper: {
      position: 'relative'
    },
    slideGraphic: {
      width: 1.6,
      height: 1,
      marginRight: 0.3
    },
    slideText: {
      position: 'absolute',
      top: 0.88,
      right: 0.35,
      fontWeight: '400',
      color: 'white',
      opacity: this.state.textOpacity,
      transform: [{ translateZ: 0.2 }]
    }
  };

  componentWillMount() {
    // set label's font size
    if (this.props.text) {
      this._style.slideText.fontSize = ScreenModule.isMobile ? 0.2 : 0.15;
    }

    // create video controls (video slide)
    if (this.is.videoSlide) {
      this.setState({
        playerState: new MediaPlayerState({ autoPlay: false, muted: true })
      });
    }
  }

  componentDidMount() {
    const { onLoad } = this.props;

    // if callback is set, run it when video is loaded
    if (onLoad && this.is.videoSlide) {
      let resolveTimer, videoLoadState = 0;

      // give 6s to resolve, otherwise - finish preloader
      resolveTimer = setTimeout(onLoad, 6000);

      this.state.playerState.onPlayStatusChange = () => {
        videoLoadState += 1;
        if (videoLoadState === 2) {
          // wait 2.5s more (make sure to load assets)
          clearTimeout(resolveTimer);
          setTimeout(onLoad, 2000);
        }
      };
    }
  }

  /**
   * Detect slide type
   *
   * @public
   * @get
   * @return {{videoSlide: boolean, imageSlide: boolean}}
   */
  get is() {
    const { props: { type } } = this;

    return {
      videoSlide: type === 'video',
      imageSlide: type === 'image'
    };
  }

  /**
   * Generate core slide element based on type
   *
   * @private
   * @return {XML}
   */
  get _slideGraphicElement() {
    const { state, props, _style } = this;
    const settings = {
      source: {
        uri: props.src,
        format: 'mp4'
      },
      style: _style.slideGraphic
    };

    if (this.is.imageSlide) {
      const { onLoad } = props;

      settings.resizeMode = 'cover';
      if (onLoad) settings.onLoadEnd = onLoad;

      return <Image {...settings} />;
    }

    if (this.is.videoSlide) {
      const { poster: posterURL } = props;

      settings.playerState = state.playerState;
      if (posterURL) settings.poster = { uri: posterURL };

      return <Video loop {...settings} />;
    }
  }

  render() {
    const { props, _style, _slideGraphicElement } = this;

    return (
      <Animated.View style={_style.slideWrapper}>
        {_slideGraphicElement}
        {props.audio &&
        <Sound
          source={{
            uri: props.audio
          }}
          loop
          volume={0.2}
        />}
        {props.text &&
        <Animated.Text style={_style.slideText}>
          {props.text.toUpperCase()}
        </Animated.Text>}
      </Animated.View>
    );
  }
}
