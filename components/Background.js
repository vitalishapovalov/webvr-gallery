import React from 'react';
import { Pano } from 'react-vr';
import { BACKGROUND_IMG_SRC } from '../helpers/_constants';

export default function Background() {
  return <Pano source={{
    uri: BACKGROUND_IMG_SRC
  }} />;
};
