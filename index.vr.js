import { AppRegistry } from 'react-vr';
import App from './components/App';

/**
 * @TODO bitmap font (Roboto Condensed)
 */
AppRegistry.registerComponent('VintagePortfolio', () => App);
