/**
 * Helper class used to get info about screen.
 *
 * @class Screen
 */
export class Screen {
  /**
   * Detect mobile device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isMobile() {
    return window.matchMedia('(max-width: 1024px)').matches;
  }

  /**
   * Detect desktop device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isDesktop() {
    return !Screen.isMobile;
  }
}

/**
 * Get browser's current language.
 *
 * @constant
 * @type {String}
 */
export const browserLanguage = navigator.language || navigator.userLanguage;

/**
 * Resize VR canvas DOM element.
 *
 * @param {Object} vr
 * @param {Boolean} isPolyfilled
 * @param {Object|Null} vrDisplay
 */
export function resizePolyfilled(vr, isPolyfilled, vrDisplay) {
  if (!isPolyfilled || (isPolyfilled && !vrDisplay.isPresenting)) return;

  const { innerWidth: width, innerHeight: height } = window;

  if (parseInt(vr.player.glRenderer.domElement.style.width, 10) !== width) {
    vr.player.glRenderer.domElement.style.width = `${width}px`;
    vr.player.glRenderer.domElement.style.height = `${height}px`;
    vr.player.glRenderer.domElement.style.paddingRight = 0;
    vr.player.glRenderer.domElement.style.paddingBottom = 0;
  }
}

/**
 * Toggle full-screen mode.
 *
 * @param {HTMLElement} [element=document.querySelector('canvas')]
 */
export function toggleFullScreen(element = document.querySelector('canvas')) {
  if (!document.fullscreenElement &&
    !document.mozFullScreenElement && !document.webkitFullscreenElement) {
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (element.cancelFullScreen) {
      element.cancelFullScreen();
    } else if (element.mozCancelFullScreen) {
      element.mozCancelFullScreen();
    } else if (element.webkitCancelFullScreen) {
      element.webkitCancelFullScreen();
    }
  }
}
