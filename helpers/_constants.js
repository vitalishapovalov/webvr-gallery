/**
 * @constant
 * @type {String}
 */
export const BACKGROUND_IMG_SRC = '/static_assets/vintage_BG.png';

/**
 * @constant
 * @type {String}
 */
export const ARROW_PREV_IMG_SRC = '/static_assets/arrow-back.png';

/**
 * @constant
 * @type {String}
 */
export const ARROW_NEXT_IMG_SRC = '/static_assets/arrow-next.png';
