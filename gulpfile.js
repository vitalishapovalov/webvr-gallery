'use strict';

const gulp         = require('gulp');
const sass         = require('gulp-sass');
const postcss      = require('gulp-postcss');
const sourcemaps   = require('gulp-sourcemaps');
const rename       = require('gulp-rename');
const plumber      = require('gulp-plumber');
const gulpif       = require('gulp-if');
const autoprefixer = require('autoprefixer');
const mqpacker     = require('css-mqpacker');
const notify       = require('gulp-notify');

/**
 * Set css-preprocessor files extension.
 *
 * @constant
 * @type {String}
 */
const cssPreprocessorExtension = '{scss,sass}';

/**
 * Autoprefixer options (CSS).
 * Used when compiling CSS.
 *
 * @constant
 * @type {Array}
 */
const browsers = ['last 3 versions', 'Android 4.4', 'ie 11', 'ios 8'];

/**
 * Set environment.
 * If environment wasn't set, set it to 'development'.
 *
 * @constant
 * @type {String}
 */
const NODE_ENV = process.env.NODE_ENV || 'development';

/**
 * Variables used for development/production bundles.
 * Depending on the 'NODE_ENV' variable.
 *
 * @constant
 * @type {Object}
 */
const bundle = {
  development : NODE_ENV === 'development',
  production  : NODE_ENV === 'production'
};

/**
 * Configure error handling with '_gulp-notify'.
 * Used for error handling during compilation.
 *
 * @constant
 * @type {Object}
 */
const plumberOptions = {
  errorHandler: notify.onError(error => `Error: ${error.message}`)
};

/**
 * Default CSS task.
 * Compile css-preprocessor files to '.css'.
 */
gulp.task('css-compile', () => {
  compileCss(`vr/index.${cssPreprocessorExtension}`, './vr/build/');
});

/**
 * Default gulp task.
 * Watch for changes in css-preprocessor files and compile them.
 */
gulp.task('default', () => {
  gulp.watch(`vr/index.${cssPreprocessorExtension}`, ['css-compile']);
});

/**
 * Set plugins for '_gulp-postcss'.
 *
 * @type {Array}
 */
const processors = [
  autoprefixer({
    browsers: browsers,
    cascade: false
  }),
  mqpacker({
    sort: sortMediaQueries
  })
];

/**
 * Default CSS compilation function.
 *
 * @param {String} src
 * @param {String} dest
 */
function compileCss(src, dest) {
  gulp
    .src(src)
    .pipe(plumber(plumberOptions))
    .pipe(gulpif(!bundle.production, sourcemaps.init()))
    .pipe(sass({
      outputStyle: bundle.production ? 'compressed' : 'expanded',
      precision: 5
    }))
    .pipe(postcss(processors))
    .pipe(rename({
      suffix: '.min',
      extname: '.css'
    }))
    .pipe(gulpif(!bundle.production, sourcemaps.write()))
    .pipe(gulp.dest(dest));
}

/**
 * Detect 'max-width' media query.
 *
 * @param {String} mq
 * @return {boolean}
 */
function isMax(mq) {
  return /max-width/.test(mq);
}

/**
 * Detect 'min-width' media query.
 *
 * @param {String} mq
 * @return {boolean}
 */
function isMin(mq) {
  return /min-width/.test(mq);
}

/**
 * Sort media queries (max/min width).
 *
 * @param {String} a
 * @param {String} b
 * @return {Number}
 */
function sortMediaQueries(a, b) {
  const A = a.replace(/\D/g, '');
  const B = b.replace(/\D/g, '');

  if (isMax(a) && isMax(b)) {
    return B - A;
  } else if (isMin(a) && isMin(b)) {
    return A - B;
  } else if (isMax(a) && isMin(b)) {
    return 1;
  } else if (isMin(a) && isMax(b)) {
    return -1;
  }

  return 1;
}

/** Compile on first run (development) */
if (bundle.development)
  compileCss(`vr/index.${cssPreprocessorExtension}`, './vr/build/');