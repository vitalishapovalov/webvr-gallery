import { Module } from 'react-vr-web';
import { TweenLite } from 'gsap';
import Instruction from '../modules/Instruction';
import { Screen } from '../helpers/_helpers';

/**
 * @class PreloaderModule
 */
class PreloaderModule extends Module {
  /**
   * @constructor
   */
  constructor() {
    super('PreloaderModule');

    this._el = document.getElementById('preloader');

    this._Instruction = new Instruction();
  }

  /**
   * @public
   */
  finishPreloaderAndRunInstruction() {
    TweenLite.to(this._el, 1, { opacity: 0, onComplete: this._destroyPreloader });
  }

  /**
   * @private
   * @this PreloaderModule
   */
  _destroyPreloader = () => {
    this._el.remove();

    Screen.isMobile ? this._runInstruction() : this._destroyInstruction();
  };

  /**
   * @private
   * @this PreloaderModule
   */
  _runInstruction = () => {
    this._Instruction.run();
  };

  /**
   * @private
   * @this PreloaderModule
   */
  _destroyInstruction = () => {
    this._Instruction.destroy();
  };
}

export default new PreloaderModule;
