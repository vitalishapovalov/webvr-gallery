import { Module } from 'react-vr-web';
import { Screen } from '../helpers/_helpers';

/**
 * @class ScreenModule
 */
class ScreenModule extends Module {
  /**
   * @constructor
   */
  constructor() {
    super('ScreenModule');

    this.isMobile = Screen.isMobile;
    this.isDesktop = Screen.isDesktop;
  }
}

export default new ScreenModule;
