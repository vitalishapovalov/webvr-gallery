import 'webvr-polyfill';
import { VRInstance } from 'react-vr-web';
import { resizePolyfilled } from '../helpers/_helpers';
import PreloaderModule from '../native_modules/PreloaderModule';
import ScreenModule from '../native_modules/ScreenModule';
import Interface from '../modules/Interface';

// 'full screen' button etc.
new Interface().bindListeners();

// WebVR polyfill
WebVRPolyfill.InstallWebVRSpecShim();
let isPolyfilled = false;
let vrDisplay = null;
navigator.getVRDisplays()
  .then(([display]) => { if (display) vrDisplay = display; });

// Main 'init' function
function init(bundle, parent, options) {
  const vr = new VRInstance(bundle, 'VintagePortfolio', parent, {
    nativeModules: [PreloaderModule, ScreenModule],
    ...options
  });

  vr.render = () => {
    resizePolyfilled(vr, isPolyfilled, vrDisplay);
  };

  vr.start();

  return vr;
}

window.ReactVR = { init };
