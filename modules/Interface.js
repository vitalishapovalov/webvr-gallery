import { toggleFullScreen } from '../helpers/_helpers';

/**
 * @class Interface
 */
export default class Interface {
  /**
   * @constructor
   */
  constructor() {
    this.fullScreen = document.getElementById('fullScreen');
  }

  /**
   * @public
   * @return {Interface}
   */
  bindListeners() {
    this.fullScreen
      .addEventListener('click', this._requestFullScreen);

    return this;
  }

  /**
   * @public
   * @static
   * @get
   * @return {Element}
   */
  static get canvas() {
    return document.querySelector('canvas');
  }

  /**
   * @private
   * @this Interface
   */
  _requestFullScreen = () => {
    toggleFullScreen(Interface.canvas);
  };
}
