import { TweenLite, TimelineLite, Power1 } from 'gsap';
import bodymovin from 'bodymovin';
import animationData from '../json/instruction.json';
import SplitTextPlugin from '../helpers/_splitText';

// SplitText plugin activation
SplitTextPlugin(window);

/**
 * @class Instruction
 */
export default class Instruction {
  /**
   * @type {HTMLElement}
   * @private
   */
  _instructionContainer = document.getElementById('instruction');

  /**
   * @type {HTMLElement}
   * @private
   */
  _animationContainer = this._instructionContainer.querySelector('.instruction__animation');

  /**
   * @type {NodeList}
   * @private
   */
  _textParts = this._instructionContainer.querySelectorAll('.instruction__text-part');

  /**
   * @type {Boolean}
   * @private
   */
  _isStarted = false;

  /**
   * @type {Object}
   * @private
   */
  _bodymovinData = {
    animationData,
    container: this._animationContainer,
    renderer: 'svg',
    loop: false,
    autoplay: false
  };

  /**
   * @type {Null|bodymovin}
   * @private
   */
  _iconAnimation = null;

  /**
   * @type {TimelineLite}
   * @private
   */
  _textAnimation = new TimelineLite({ paused: true })
    .staggerFromTo(this._textParts, 0.5, {
      opacity: 0,
      y: 19
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut
    }, 0.15);

  /**
   * @public
   * @return {Instruction}
   */
  run() {
    this
      ._runIconAnimation()
      ._showText();

    return this;
  }

  /**
   * @public
   * @this Instruction
   * @return {Instruction}
   */
  destroy = () => {
    this._instructionContainer.remove();

    return this;
  };

  /**
   * @private
   * @return {Instruction}
   */
  _runIconAnimation() {
    // run only once
    if (this._isStarted) return this;

    this._isStarted = true;

    this._iconAnimation = bodymovin.loadAnimation(this._bodymovinData);

    this._iconAnimation.addEventListener('complete', this._hideAndDestroyInstruction);

    this._iconAnimation.addEventListener('enterFrame', ({ currentTime }) => {
      if (currentTime > 120) this._hideText();
    });

    this._iconAnimation.play();

    return this;
  }

  /**
   * @private
   * @return {Instruction}
   */
  _showText() {
    this._textAnimation.play();

    return this;
  }

  /**
   * @private
   * @return {Instruction}
   */
  _hideText() {
    this._textAnimation.reverse();

    return this;
  }

  /**
   * @private
   * @this Instruction
   * @return {Instruction}
   */
  _hideAndDestroyInstruction = () => {
    TweenLite.to(this._instructionContainer, 0.8, {
      opacity: 0,
      onComplete: this.destroy
    });

    return this;
  };
}
