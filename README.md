# Vintage WebVR experiment

## Demo

[Online preview](http://markup.com.dev11.vintagedev.com.ua/vr/)

## Built with

* [ReactVR](https://facebook.github.io/react-vr/)
* [WebVR Polyfill](https://github.com/googlevr/webvr-polyfill)